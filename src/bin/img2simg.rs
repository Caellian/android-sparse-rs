extern crate android_sparse as sparse;

use clap::{crate_authors, crate_version, App, Arg, ArgMatches};
use std::{fs::File, process};

fn parse_args() -> ArgMatches {
    App::new("img2simg")
        .about("Encode a raw image to a sparse image")
        .version(crate_version!())
        .author(crate_authors!())
        .arg(
            Arg::new("raw_image")
                .help("Path of the raw image")
                .required(true),
        )
        .arg(
            Arg::new("sparse_image")
                .help("Path of the output sparse image")
                .required(true),
        )
        .arg(
            Arg::new("crc")
                .help("Add a checksum to the sparse image")
                .short('c')
                .long("crc"),
        )
        .get_matches()
}

fn img2simg(args: &ArgMatches) -> sparse::Result<()> {
    let fi = File::open(&args.value_of("raw_image").unwrap())?;
    let fo = File::create(&args.value_of("sparse_image").unwrap())?;

    let encoder = sparse::Encoder::new(fi)?;

    let mut writer = if args.is_present("crc") {
        sparse::Writer::with_crc(fo)?
    } else {
        sparse::Writer::new(fo)?
    };

    for block in encoder {
        writer.write_block(&block?)?;
    }

    Ok(())
}

fn main() {
    let args = parse_args();
    img2simg(&args).unwrap_or_else(|err| {
        eprintln!("error: {}", err);
        process::exit(1);
    });
}
