//! Extensions for foreign types.

use crate::block::Block;

/// Enables writing sparse blocks to `crc::Digest`s.
pub trait WriteBlock {
    /// Writes passed block to `crc::Digest` and returns number of hashed bytes.
    fn write_block(&mut self, block: &Block) -> usize;
}

impl<'a> WriteBlock for crc::Digest<'a, u32> {
    fn write_block(&mut self, block: &Block) -> usize {
        match block {
            Block::Raw(buf) => {
                self.update(&**buf);
                buf.len()
            }
            Block::Fill(value) => {
                for _ in 0..(Block::SIZE / 4) {
                    self.update(value);
                }
                Block::SIZE as usize
            }
            Block::Skip => {
                self.update(&[0; Block::SIZE as usize]);
                Block::SIZE as usize
            }
            Block::Crc32(_) => unreachable!("don't hash digest blocks"),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::CRC;

    fn block_crc(block: &Block) -> u32 {
        let mut digest = CRC.digest();
        digest.write_block(block);
        digest.finalize()
    }

    #[test]
    fn crc_write_raw_block() {
        let block = Block::Raw(Box::new([b'A'; Block::SIZE as usize]));
        assert_eq!(block_crc(&block), 0xfea63440);
    }

    #[test]
    fn crc_write_fill_block() {
        let block = Block::Fill([b'A'; 4]);
        assert_eq!(block_crc(&block), 0xfea63440);
    }

    #[test]
    fn crc_write_skip_block() {
        let block = Block::Skip;
        assert_eq!(block_crc(&block), 0xc71c0011);
    }

    #[test]
    fn crc_write_crc32_block() {
        let block = Block::Crc32(0x12345678);
        assert_eq!(block_crc(&block), 0);
    }
}
