//! An implementation of Android's sparse file format.
//!
//! Enables reading and writing sparse images, as well as encoding from and
//! decoding to raw images:
//!
//! ```text
//!  --------               --------                -------
//! | sparse | --Reader--> | sparse | --Decoder--> | raw   |
//! | image  | <--Writer-- | blocks | <--Encoder-- | image |
//!  --------               --------                -------
//! ```

#![deny(missing_docs)]

pub mod block;
pub mod read;
pub mod result;
pub mod write;

pub mod ext;
mod headers;

use crc::{Crc, CRC_32_CKSUM};
pub(crate) static CRC: Crc<u32> = Crc::<u32>::new(&CRC_32_CKSUM);
pub(crate) use self::ext::*;

pub use self::{
    block::Block,
    read::{Encoder, Reader},
    result::Result,
    write::{Decoder, Writer},
};
